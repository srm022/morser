﻿using Morser.Utility;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Morser.Handlers
{
    public abstract class BaseSignalHandler : IBaseSignalHandler
    {
        protected List<List<int>> Words;
        protected List<List<string>> Sounds;

        protected List<int> LightSequence;
        protected List<string> SoundSequence;

        protected string DotFx;
        protected string DashFx;
        protected string LetterGapSignalFx;

        protected const int DotSignal = 1;
        protected const int DashSignal = DotSignal * 2;
        protected const int LetterGapSignal = DotSignal * 4;

        protected const int SignalLength = 300;
        private const int MorseCodeLetterGapLength = 500;
        private const int LiteralLetterGapLength = 700;
        private const int WordGapLength = 1000;

        public abstract Task SendLightSignal(int sequenceLength);
        public abstract Task SendSoundSignal(string soundSource);

        protected BaseSignalHandler()
        {
            DotFx = "dot.mp3";
            DashFx = "dash.mp3";
            LetterGapSignalFx = "";
        }

        public void PrepareSignalSequence(string morseCode)
        {
            InitializeLists();

            foreach (var character in morseCode)
            {
                switch (character)
                {
                    case MorseCodeDictionary.Dot:
                        LightSequence.Add(DotSignal);
                        SoundSequence.Add(DotFx);
                        break;

                    case MorseCodeDictionary.Dash:
                        LightSequence.Add(DashSignal);
                        SoundSequence.Add(DashFx);
                        break;

                    case MorseCodeDictionary.Space:
                        LightSequence.Add(LetterGapSignal);
                        SoundSequence.Add(LetterGapSignalFx);
                        break;

                    case MorseCodeDictionary.Gap:
                        AddToWordList();
                        ResetSequenceLists();
                        break;
                }
            }

            AddToWordList();
        }

        private void AddToWordList()
        {
            Words.Add(LightSequence);
            Sounds.Add(SoundSequence);
        }

        public async void Signalize()
        {
            for (int word = 0; word < Words.Count; word++)
            {
                for (int letter = 0; letter < Words[word].Count; letter++)
                {
                    if (Words[word][letter] != LetterGapSignal)
                    {
                        var lightSignal = Signalize(SendLightSignal(Words[word][letter]));
                        var soundSignal = Signalize(SendSoundSignal(Sounds[word][letter]));

                        await Task.WhenAll(lightSignal, soundSignal);

                        Thread.Sleep(MorseCodeLetterGapLength);
                    }
                    else
                        Thread.Sleep(LiteralLetterGapLength);
                }

                Thread.Sleep(WordGapLength);
            }
        }

        private Task Signalize(Task task)
        {
            return SignalInvokeMethodFactory.GetSignalAction(task);
        }

        protected void InitializeLists()
        {
            Words = new List<List<int>>();
            Sounds = new List<List<string>>();
            ResetSequenceLists();
        }

        private void ResetSequenceLists()
        {
            LightSequence = new List<int>();
            SoundSequence = new List<string>();
        }
    }
}