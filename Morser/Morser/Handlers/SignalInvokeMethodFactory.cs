﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Morser.Handlers
{
    public static class SignalInvokeMethodFactory
    {
        public static Task GetSignalAction(Task task)
        {
            Task signalAction;
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    signalAction = Task.Run(() => task);
                    break;

                case Device.UWP:
                    signalAction = task;
                    break;

                default:
                    throw new NotImplementedException("This device is not supported! " + Device.RuntimePlatform);
            }

            return signalAction;
        }
    }
}