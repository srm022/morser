﻿namespace Morser.Handlers
{
    public interface IBaseSignalHandler
    {
        void PrepareSignalSequence(string morseCode);
        void Signalize();
    }
}