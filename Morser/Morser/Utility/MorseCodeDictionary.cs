﻿using System;
using System.Collections.Generic;

namespace Morser.Utility
{
    public static class MorseCodeDictionary
    {
        public const char Dot = '•';
        public const char Dash = '−';
        public const char Space = ' ';
        public const char Gap = '/';

        public static readonly Dictionary<char, string> Dictionary = new Dictionary<char, string>
            {
                {'a', String.Concat(Dot, Dash)},
                {'b', String.Concat(Dash, Dot, Dot, Dot)},
                {'c', String.Concat(Dash, Dot, Dash, Dot)},
                {'d', String.Concat(Dash, Dot, Dot)},
                {'e', String.Concat(Dot)},
                {'f', String.Concat(Dot, Dot, Dash, Dot)},
                {'g', String.Concat(Dash, Dash, Dot)},
                {'h', String.Concat(Dot, Dot, Dot, Dot)},
                {'i', String.Concat(Dot, Dot)},
                {'j', String.Concat(Dot, Dash, Dash, Dash)},
                {'k', String.Concat(Dash, Dot, Dash)},
                {'l', String.Concat(Dot, Dash, Dot, Dot)},
                {'m', String.Concat(Dash, Dash)},
                {'n', String.Concat(Dash, Dot)},
                {'o', String.Concat(Dash, Dash, Dash)},
                {'p', String.Concat(Dot, Dash, Dash, Dot)},
                {'q', String.Concat(Dash, Dash, Dot, Dash)},
                {'r', String.Concat(Dot, Dash, Dot)},
                {'s', String.Concat(Dot, Dot, Dot)},
                {'t', String.Concat(Dash)},
                {'u', String.Concat(Dot, Dot, Dash)},
                {'v', String.Concat(Dot, Dot, Dot, Dash)},
                {'w', String.Concat(Dot, Dash, Dash)},
                {'x', String.Concat(Dash, Dot, Dot, Dash)},
                {'y', String.Concat(Dash, Dot, Dash, Dash)},
                {'z', String.Concat(Dash, Dash, Dot, Dot)},
                {' ', String.Concat(Gap)}
            };
    }
}