﻿namespace Morser.Utility
{
    public class Converter
    {
        public string ConvertToMorseCode(string inputText)
        {
            string convertedText = null;

            foreach (char character in inputText)
                convertedText += GetCodeForCharacter(character) + MorseCodeDictionary.Space;

            return convertedText;
        }

        private string GetCodeForCharacter(char character)
        {
            return MorseCodeDictionary.Dictionary.TryGetValue(character, out var convertedText) ? convertedText : "?";
        }
    }
}