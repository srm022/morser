﻿using Morser.Handlers;
using Xamarin.Forms;

namespace Morser
{
    public partial class App : Application
    {
        public App(IBaseSignalHandler signalHandler)
        {
            InitializeComponent();
            
            MainPage = new Morser.MainPage(signalHandler);
        }
    }
}