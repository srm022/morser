﻿using Morser.Handlers;
using Morser.Utility;
using Plugin.Compass;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Morser
{
    public partial class MainPage
    {
        private readonly Converter _converter;
        private readonly IBaseSignalHandler _signalHandler;

        public MainPage(IBaseSignalHandler signalHandler)
        {
            InitializeComponent();
            _converter = new Converter();
            _signalHandler = signalHandler;

            UpdateCompass();
        }

        private void TextSubmit(object sender, EventArgs e)
        {
            var morseCode = _converter.ConvertToMorseCode(InputField.Text);

            OutputField.Text = morseCode;
            _signalHandler.PrepareSignalSequence(morseCode);

            FlashSignalsButton.IsVisible = true;
        }

        private void SendFlashSignals(object sender, EventArgs e)
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    _signalHandler.Signalize();
                    break;

                case Device.UWP:
                    Task.Run(() => _signalHandler.Signalize());
                    break;
            }
        }

        private void ResetOutput(object sender, FocusEventArgs e)
        {
            OutputField.Text = "";
            FlashSignalsButton.IsVisible = false;
        }

        private void UpdateCompass()
        {
            CrossCompass.Current.CompassChanged += (s, e) =>
            {
                CompassOutput.Text = e.Heading.ToString("####") + "°";
                Compass.Rotation = -e.Heading;
            };

            CrossCompass.Current.Start();
        }
    }
}