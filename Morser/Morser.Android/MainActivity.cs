﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Morser.Droid.Handlers;
using Plugin.Permissions;
using Permission = Plugin.Permissions.Abstractions.Permission;

namespace Morser.Droid
{
    [Activity(Label = "Morser", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Xamarin.Essentials.Platform.Init(this, bundle);
            Xamarin.Forms.Forms.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
            
            LoadApplication(new App(new AndroidSignalHandler()));
        }
    }
}

