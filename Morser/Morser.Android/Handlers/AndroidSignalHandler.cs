﻿using Morser.Handlers;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Morser.Droid.Handlers
{
    public class AndroidSignalHandler : BaseSignalHandler
    {
        public override async Task SendLightSignal(int sequenceLength)
        {
            await Flashlight.TurnOnAsync();
            Thread.Sleep(sequenceLength * SignalLength);
            await Flashlight.TurnOffAsync();
        }

        public override Task SendSoundSignal(string soundSource)
        {
            if (soundSource != LetterGapSignalFx)
            {

                var audioPlayer = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;

                audioPlayer.Load(soundSource);
                audioPlayer.Play();
            }

            return Task.CompletedTask;
        }
    }
}