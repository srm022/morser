﻿using Morser.Handlers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Xamarin.Forms;

namespace Morser.UWP.Handlers
{
    public class UWPSignalHandler : BaseSignalHandler
    {
        private BoxView _flashlightBox;

        public override async Task SendLightSignal(int sequenceLength)
        {
            _flashlightBox = Application.Current.MainPage.FindByName<BoxView>("FlashlightBox");

            await ChangeBackgroundColor(Color.White);
            Thread.Sleep(sequenceLength * SignalLength);
            await ChangeBackgroundColor(Color.Black);
        }

        private IAsyncAction ChangeBackgroundColor(Color color)
        {
            return Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                () => _flashlightBox.Color = color
            );
        }

        public override async Task SendSoundSignal(string soundSource)
        {
            await Task.Run(() =>
            {
                if (soundSource != LetterGapSignalFx)
                {
                    var audioPlayer = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;

                    audioPlayer.Load(soundSource);
                    audioPlayer.Play();
                }
            });
        }
    }
}